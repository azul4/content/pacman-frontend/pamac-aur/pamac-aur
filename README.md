# pamac-aur

A Gtk3 frontend, Package Manager based on libalpm with AUR and Appstream support

https://gitlab.manjaro.org/applications/pamac

<br>

How to clone this repository:
```
git clone https://gitlab.com/azul4/content/pacman-frontend/pamac-aur/pamac-aur.git
```

